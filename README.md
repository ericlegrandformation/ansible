# ANSIBLE - Industrialiser les déploiements

![a](https://i2.wp.com/codeblog.dotsandbrackets.com/wp-content/uploads/2017/09/ansible-vagrant.jpg?zoomresize=480%2C240)

Ce projet va vous permettre de :
 * Connaître les caractéristiques et le fonctionnement d'Ansible
 * Savoir écrire des playbooks (scripts) pour orchestrer des opérations
 * Comprendre comment tirer parti de la solution pour optimiser le pilotage d’un parc et le déploiement d’applications

Il va s'articuler autour de 13 LABS:
  * LAB #1  - Création/Importation de vos Machines Virtuelles,
  * LAB #2  - Connexion à vos Machines Virtuelles,
  * LAB #3  - Installation d'Ansible,
  * LAB #4  - Configuration de SSH et de sudo,
  * LAB #5  - Débuter avec Ansible,
  * LAB #6  - La Commande ansible-playbook,
  * LAB #7  - La Commande ansible-galaxy,
  * LAB #8  - Dépendances de Rôles,
  * LAB #9  - Utilisation de Gabarits,
  * LAB #10 - Gestion de la Hiérarchie des Variables,
  * LAB #11 - Utilisation des Facts d'Ansible,
  * LAB #12 - La Commande ansible-vault,
  * LAB #13 - Ansible et Docker.

## Pour commencer

Ce projet a été réalisé avec un ordinateur disposant de 16Go de RAM disposant d'au moins 60 Go d'espace disque afin d'être à l'aise lors de la création et l'utilisation des VM.

### Pré-requis

Ce qui est requis pour commencer avec ce projet...

- Un ordinateur connecté à internet préférablement avec Linux
- des connaissances de base en système Linux, réseaux informatique et applications Web
- des notions de programmation (scripting, Python, PHP, ..)
- avoir déjà utilisé Git, VirtualBox et un outil de Dev (par exemple VSCode) seront un plus utile

### Installation

Les 13 LABS sont prévus pour être réalisés à partir de VM VirtualBox non disponible dans ce projet.
La première approche de réalisation consiste donc à effectuer "à la main" les manipulations proposées sur les VM qui seront fournies. 
Cette manière de faire permet de appréhender en profondeur tous les aspects de l'administration système. 

Une seconde approche consiste à créer, avec Vagrant et Ansible, les VM VirtualBox dont vous allez avoir besoin. C'est ce qui est proposé de faire ici en couvrant les LABs 1 à 4. Les LABs 5 à 13 seront à réaliser à partir des VM ainsi créées.
Cette autre manière de faire permet de mettre directement en pratique les principes de l'Infrastructure As Code (IAC).
 
Un LAB 14 en bonus permet de déployer l'infrastructure mise au point localement dans le cloud (sur Digital Ocean par exemple).

## Démarrage

Vous êtes donc dans la seconde approche consistant à créer, avec Vagrant et Ansible, les VM VirtualBox dont vous allez avoir besoin.

Sur votre machine hôte, depuis votre répertoire **/home/'votre nom utilisateur'**, dans un terminal, taper successivement :
* **git clone** **https://framagit.org/ericlegrandformation/ansible.git** 
* ouvrer le dossier "ansible" **avec VSCode**
* ouvrer un **nouveau terminal** dans VSCode
* créer un environnement virtuel python avec la commande "**virtualenv --python=python3.5 venv**"
* activer l'environnement virtuel python avec la commande "**source ./venv/bin/activate**"
* installer quelques pré-requis avec la commande "**sudo apt install libffi-dev python-dev libssl-dev**"
* installer ansible avec la commande "**pip install ansible**"
* vérifier la version ansible avec "**ansible --version**" (version 2.8 mini)
* Lancer la création des VM VirtualBox avec la commande "**vagrant up**"
* Configurer la VM Ansible avec la commande "**ansible-playbook setup_ansible.yml**"
* Configurer les VM TargetA et TargetB avec la commande "**ansible-playbook setup_target.yml**"
* Metter à jour les fichiers /etc/hosts des 3 machines avec "**ansible-playbook updatehosts.yml**"
* Configurer l'accès sécuriser ssh des 3 machines avec "**ansible-playbook sshkeys-exchange**"
* **Redémarrer les 3 machines** depuis VirtualBox
* Vérifier la configuration des 3 machines en visualisant tous les fichiers utiles après vous être connecter en ssh avec les commandes "**vagrant ssh ansible**" puis "**vagrant ssh targeta**" puis "**vagrant ssh targetb**"



*Bravo, vous venez de faire les LABs 1 à 4.*

***

## Versions

**Dernière version stable :** 0.1
**Dernière version :** 0.1
Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/ansible.git)

## Auteurs

* **Eric Legrand** _alias_ [@ericlegrand](https://framagit.org/ericlegrandformation/ansible.git)
avec le soutien de Messieurs Hugh Norris et Elie Gavoty.